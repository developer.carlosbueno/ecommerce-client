import React from 'react'
import { Link } from 'react-router-dom'

export interface LogoProps {
  img?: string
  imgLight?: string
  className?: string
}

const Logo: React.FC<LogoProps> = ({
  img = 'https://ciseco-reactjs.vercel.app/static/media/logo.95d47bbac8db6c1e8f997bbf26ca05cf.svg',
  imgLight = 'https://ciseco-reactjs.vercel.app/static/media/logo-light.addd474f214aa6f412a83d0ccee14f55.svg',
  className = 'flex-shrink-0',
}) => {
  return (
    <Link
      to='/home'
      className={`ttnc-logo inline-block text-slate-600 ${className}`}
    >
      {/* THIS USE FOR MY CLIENT */}
      {/* PLEASE UN COMMENT BELLOW CODE AND USE IT */}
      {img ? (
        <img
          className={`block max-h-8 sm:max-h-10 ${
            imgLight ? 'dark:hidden' : ''
          }`}
          src={img}
          alt='Logo'
        />
      ) : (
        'Logo Here'
      )}
      {imgLight && (
        <img
          className='hidden max-h-8 sm:max-h-10 dark:block'
          src={imgLight}
          alt='Logo-Light'
        />
      )}
    </Link>
  )
}

export default Logo
