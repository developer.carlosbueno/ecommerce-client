import { FC } from 'react'
import { Link } from 'react-router-dom'
import ButtonSecondary from '@components/shared/Button/ButtonSecondary'
import { CATS_DISCOVER } from '@components/DiscoverMoreSlider'

export interface CardCategory3Props {
  className?: string
  featuredImage?: string
  name?: string
  desc?: string
  color?: string
}

const CardCategory3: FC<CardCategory3Props> = ({
  className = '',
  featuredImage = CATS_DISCOVER[2].featuredImage,
  name = CATS_DISCOVER[2].name,
  desc = CATS_DISCOVER[2].desc,
  color = CATS_DISCOVER[2].color,
}) => {
  return (
    <Link
      to={'/page-collection'}
      className={`nc-CardCategory3 block h-full ${className}`}
      data-nc-id='CardCategory3'
    >
      <div
        className={`relative w-full h-full aspect-w-16 aspect-h-11 sm:aspect-h-9 rounded-2xl overflow-hidden group ${color}`}
      >
        <div>
          <img
            src={featuredImage}
            className='absolute right-0 w-1/2 max-w-[260px] h-full object-contain drop-shadow-xl'
          />
        </div>
        <span className='opacity-0 group-hover:opacity-40 absolute inset-0 bg-black/10 transition-opacity'></span>

        <div>
          <div className='absolute inset-5 sm:inset-8 flex flex-col'>
            <div className='max-w-xs'>
              <span className={`block mb-2 text-sm text-slate-700`}>
                {name}
              </span>
              {desc && (
                <h2
                  className={`text-xl md:text-2xl text-slate-900 font-semibold`}
                  dangerouslySetInnerHTML={{ __html: desc }}
                ></h2>
              )}
            </div>
            <div className='mt-auto'>
              <ButtonSecondary
                sizeClass='py-3 px-4 sm:py-3.5 sm:px-6'
                fontSize='text-sm font-medium'
                className='nc-shadow-lg'
              >
                Show me all
              </ButtonSecondary>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default CardCategory3

// import React from 'react'
// import './styles.css'

// interface ICategoryCardProps {
//   children?: React.ReactNode
//   image?: string
// }

// const CategoryCard: React.FC<ICategoryCardProps> = ({ image }) => {
//   return (
//     <div
//       className={`relative aspect-w-12 aspect-h-11 rounded-3xl overflow-hidden bg-white dark:bg-neutral-900 hover:shadow-lg transition-shadow w-[440px] h-[350px]`}
//     >
//       <div className='absolute inset-5 sm:inset-8 flex flex-col justify-between'>
//         {/* Header */}
//         <div className='flex justify-between items-center'>
//           <div className='nc-NcImage w-20 h-20 rounded-full overflow-hidden z-0 bg-indigo-50'>
//             <img
//               src={image}
//               className='object-cover w-full h-full'
//               alt='nc-imgs'
//             />
//           </div>
//           <span className='text-xs text-slate-700 dark:text-neutral-300 font-medium'>
//             292 products
//           </span>
//         </div>

//         {/* Mid */}
//         <div>
//           <span className='block mb-2 text-sm text-slate-500 dark:text-slate-400'>
//             Manufacturar
//           </span>
//           <h2 className='text-2xl sm:text-3xl font-semibold'>Backpack</h2>
//         </div>

//         <a
//           className='flex items-center text-sm font-medium hover:text-primary-500 transition-colors'
//           href='/page-collection'
//         >
//           <span>See Collection</span>
//           <svg
//             xmlns='http://www.w3.org/2000/svg'
//             fill='none'
//             viewBox='0 0 24 24'
//             stroke-width='1.5'
//             stroke='currentColor'
//             aria-hidden='true'
//             className='w-4 h-4 ml-2.5'
//           >
//             <path
//               stroke-linecap='round'
//               stroke-linejoin='round'
//               d='M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3'
//             ></path>
//           </svg>
//         </a>
//       </div>
//     </div>
//   )
// }

// export default CategoryCard
