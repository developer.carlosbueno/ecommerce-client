import React from 'react'
import './styles.css'

interface IOfertCardProps {
  children?: React.ReactNode
  color: string
  image: string
}

const OfertCard: React.FC<IOfertCardProps> = ({ color, image }) => {
  return (
    <div
      className={`relative w-[490px] h-[275px] ${color} rounded-2xl overflow-hidden shadow-sm ml-[10px]`}
    >
      <div className='absolute inset-5 sm:inset-8'>
        <div className='relative'>
          <div className='max-w-xs mb-14'>
            <span className='block mb-2 text-sm text-slate-700'>
              Explore new arrivals
            </span>
            <h2 className='text-xl md:text-2xl text-slate-900 font-semibold'>
              Shop the latest <br /> from top brands
            </h2>
          </div>

          <div className='mt-auto z-50'>
            <button className='cursor-pointer relative h-auto inline-flex items-center justify-center rounded-full transition-colors text-sm font-medium py-3 px-4 sm:py-3.5 sm:px-6  ttnc-ButtonSecondary bg-white text-slate-700 dark:bg-slate-900 dark:text-slate-300 hover:bg-gray-100 dark:hover:bg-slate-800 nc-shadow-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-6000 dark:focus:ring-offset-0'>
              Show me all
            </button>
          </div>
          <img
            src={image}
            alt='name'
            className='absolute top-0 right-0 w-1/2 max-w-[260px] h-full object-contain drop-shadow-xl'
          />
        </div>
      </div>
      <span className='z-10 opacity-0 hover:opacity-40 absolute inset-0 bg-black/10 transition-opacity'></span>
    </div>

    // <li
    //   className='glide__slide glide__slide--active'
    //   style={{ width: '490px', marginRight: '10px' }}
    // >
    //   <a
    //     className='nc-CardCategory3 block '
    //     data-nc-id='CardCategory3'
    //     href='/page-collection'
    //     draggable='true'
    //   >
    //     <div className='relative w-full aspect-w-16 aspect-h-11 sm:aspect-h-9 h-0 rounded-2xl overflow-hidden group bg-yellow-50'>
    //       <div>
    //         <div
    //           className='nc-NcImage absolute inset-5 sm:inset-8'
    //           data-nc-id='NcImage'
    //         >
    //           <img
    //             src='./static/media/1.a586787f3de7735e65d3.png'
    //             className='absolute right-0 w-1/2 max-w-[260px] h-full object-contain drop-shadow-xl'
    //             alt='nc-imgs'
    //           />
    //         </div>
    //       </div>
    //       <span className='opacity-0 group-hover:opacity-40 absolute inset-0 bg-black/10 transition-opacity'></span>
    //       <div>
    //         <div className='absolute inset-5 sm:inset-8 flex flex-col'>
    //           <div className='max-w-xs'>
    //             <span className='block mb-2 text-sm text-slate-700'>
    //               Explore new arrivals
    //             </span>
    //             <h2 className='text-xl md:text-2xl text-slate-900 font-semibold'>
    //               Shop the latest
    //               <br />
    //               from top brands
    //             </h2>
    //           </div>
    //           <div className='mt-auto'>
    //             <button className='nc-Button relative h-auto inline-flex items-center justify-center rounded-full transition-colors text-sm font-medium py-3 px-4 sm:py-3.5 sm:px-6  ttnc-ButtonSecondary bg-white text-slate-700 dark:bg-slate-900 dark:text-slate-300 hover:bg-gray-100 dark:hover:bg-slate-800 nc-shadow-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-6000 dark:focus:ring-offset-0'>
    //               Show me all
    //             </button>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //   </a>
    // </li>
  )
}

export default OfertCard
