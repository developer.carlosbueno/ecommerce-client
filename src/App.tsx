import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { HelmetProvider } from 'react-helmet-async'

// CSS
import './App.css'

// PAGES
import { Home } from '@pages/index'
import ProductDetail from './pages/ProductDetail/ProductDetail'
import { Layout } from './components'
import CartPage from './pages/Cart/Cart'
import CheckoutPage from './pages/Checkout/Checkout'
import PageLogin from './pages/Login/Login'
import AccountPage from './pages/Account/Account'
import Products from './pages/Products/Products'
import AccountOrder from './pages/Account/AccountOrder'
import AccountPass from './pages/Account/AccountPass'
import AccountBilling from './pages/Account/AccountBilling'
import AccountSavelists from './pages/Account/AccountSaveList'

function App() {
  return (
    <HelmetProvider>
      <BrowserRouter>
        <Layout>
          <Routes>
            <Route path='/home' element={<Home />} />
            <Route path='/product/:id' element={<ProductDetail />} />
            <Route path='/cart' element={<CartPage />} />
            <Route path='/checkout' element={<CheckoutPage />} />
            <Route path='/login' element={<PageLogin />} />
            <Route path='/products' element={<Products />} />

            <Route path='/account' element={<AccountPage />} />
            <Route path='/account-my-order' element={<AccountOrder />} />
            <Route path='/account-change-password' element={<AccountPass />} />
            <Route path='/account-billing' element={<AccountBilling />} />
            <Route path='/account-savelists' element={<AccountSavelists />} />
          </Routes>
        </Layout>
      </BrowserRouter>
    </HelmetProvider>
  )
}

export default App
