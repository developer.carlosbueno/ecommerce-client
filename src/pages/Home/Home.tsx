import React from 'react'
import './styles.css'

// Components
import SectionHero from '@/components/SectionHero/SectionHero'
import SectionSliderProductCard from '@/components/SectionSliderProductCard'

import { PRODUCTS, SPORT_PRODUCTS } from '@data/data'
import SectionHowItWork from '@/components/SectionHowItWorks'
import DiscoverMoreSlider from '@/components/DiscoverMoreSlider'
import BackgroundSection from '@/components/BackgroundSection'
import SectionGridMoreExplore from '@/components/SectionGridMoreExplore/SectionGridMoreExplorer'
import SectionGridFeatureItems from './components/SectionGridFeatureItems'

interface IHomeProps {
  children?: React.ReactNode
}

const Home: React.FC<IHomeProps> = (props) => {
  return (
    <div className='dark:bg-slate-900'>
      {/* HERO */}
      <SectionHero />

      <div className='container overflow-hidden relative space-y-24 my-24 lg:space-y-32 lg:my-32'>
        {/* DISCOVER MORE */}
        <DiscoverMoreSlider />

        {/* HOW IT WORKS */}
        <SectionHowItWork />

        {/* SECTION SLIDER */}
        <SectionSliderProductCard
          data={[
            PRODUCTS[4],
            SPORT_PRODUCTS[5],
            PRODUCTS[7],
            SPORT_PRODUCTS[1],
            PRODUCTS[6],
          ]}
        />

        {/* SECTION */}
        <div className='relative py-24 lg:py-32'>
          <BackgroundSection />
          <SectionGridMoreExplore />
        </div>

        {/* SECTION */}
        <SectionGridFeatureItems />
      </div>
    </div>
  )
}

export default Home
